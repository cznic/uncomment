module modernc.org/uncomment

go 1.19

require (
	modernc.org/gc/v2 v2.3.0
	modernc.org/opt v0.1.3
)

require modernc.org/token v1.1.0 // indirect
