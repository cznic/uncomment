// Copyright 2022 The Uncomment Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package engine // import "modernc.org/uncomment/engine"

import (
	"bytes"
	"fmt"
	"go/token"
	"io"
	"os"
	"os/exec"
	"strings"
	"sync/atomic"

	"modernc.org/gc/v2"
	"modernc.org/opt"
)

var (
	nl  = []byte{'\n'}
	nl2 = []byte{'\n', '\n'}
	sp  = []byte{' '}
)

// Run implements the uncomment command. Please see the documentation of the
// uncomment command in the parent directory.
func Run(argv0 string, args []string, stderr io.Writer) (err error) {
	c := &cfg{argv0: argv0, stderr: stderr}
	set := opt.NewSet()
	set.Opt("gofmt", func(arg string) error { c.gofmt, err = exec.LookPath("gofmt"); return err })
	set.Opt("keep-godoc", func(string) error { c.keepGodoc = true; return nil })
	set.Opt("v", func(string) error { c.verbose = true; return nil })
	if err = set.Parse(args, func(s string) error {
		if strings.HasPrefix(s, "-") {
			return fmt.Errorf("%s: unexpected option: %s", c.argv0, s)
		}

		c.files = append(c.files, s)
		return nil
	}); err != nil {
		return fmt.Errorf("%s: %s", c.argv0, err)
	}

	p := newParallel()
	for _, v := range c.files {
		v := v
		p.exec(func() error {
			return c.uncomment(v)
		})
	}

	if err = p.wait(); err != nil {
		return err
	}

	if c.verbose {
		fmt.Fprintf(c.stderr, "<total> %d %d\n", c.in, c.out)
	}
	return nil
}

type cfg struct {
	argv0   string
	files   []string
	gofmt   string
	in, out int64
	stderr  io.Writer

	keepGodoc bool // -keep-godoc
	verbose   bool // -v
}

func (c *cfg) uncomment(fn string) (err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("%s: %s: %s", c.argv0, fn, err)
		}
	}()

	fi, err := os.Stat(fn)
	if err != nil {
		return err
	}

	in, err := os.ReadFile(fn)
	if err != nil {
		return err
	}

	atomic.AddInt64(&c.in, int64(len(in)))
	ast, err := gc.ParseSourceFile(&gc.ParseSourceFileConfig{}, fn, in)
	if err != nil {
		return err
	}

	b := bytes.NewBuffer(nil)
	b.Write(ast.PackageClause.Source(true))
	for _, v := range ast.ImportDecls {
		b.Write(v.Source(true))
	}

	for _, v := range ast.TopLevelDecls {
		if err = c.tld(b, v); err != nil {
			return err
		}
	}

	f, err := os.OpenFile(fn, os.O_WRONLY, fi.Mode())
	if err != nil {
		return err
	}

	out := b.Bytes()

	defer func() {
		if e := f.Close(); e != nil && err == nil {
			err = e
			return
		}

	}()

	if _, err = f.Write(out); err != nil {
		return err
	}

	if err = f.Truncate(int64(len(out))); err != nil {
		return err
	}

	if c.gofmt == "" {
		atomic.AddInt64(&c.out, int64(len(out)))
		if c.verbose {
			fmt.Fprintf(c.stderr, "%s %d %d\n", fn, len(in), len(out))
		}
		return nil
	}

	if err = exec.Command(c.gofmt, "-w", fn).Run(); err != nil {
		return err
	}

	if fi, err = os.Stat(fn); err != nil {
		return err
	}

	if c.verbose {
		fmt.Fprintf(c.stderr, "%s %d %d\n", fn, len(in), fi.Size())
	}
	atomic.AddInt64(&c.out, fi.Size())
	return nil
}

func (c *cfg) tld(out *bytes.Buffer, n gc.Node) error {
	toks := n.Tokens()
	if c.keepGodoc && c.isExportedTLD(toks) {
		tok := toks[0]
		if sep := tok.Sep(); len(sep) != 0 {
			sc, err := gc.NewScanner("", []byte(sep))
			if err != nil {
				return err
			}

			var comment []byte
			var noff int32
			sc.CommentHandler = func(off int32, s []byte) {
				if off != noff {
					comment = nil
				}
				comment = append(comment, s...)
				noff = off + int32(len(s))
				if l := len(s); l != 0 && s[l-1] == '/' {
					comment = append(comment, '\n')
					noff++
				}
			}
			sc.Scan()
			out.Write(nl2)
			ext := false
			if l := len(comment); l == 0 || comment[l-1] != '\n' {
				comment = append(comment, '\n')
				ext = true
			}
			if ext || int(noff) == len(sep) {
				out.Write(comment)
			}
			if l := len(comment); l != 0 && comment[l-1] != '\n' {
				out.Write(nl)
			}
			out.Write([]byte(tok.Src()))
			toks = toks[1:]
		}
	}

	for _, tok := range toks {
		switch {
		case tok.Ch == ';' && tok.Src() != ";":
			out.Write(nl)
		default:
			out.Write(c.sep(tok.Sep()))
			out.Write([]byte(tok.Src()))
		}
	}
	return nil
}

func (c *cfg) isExportedTLD(toks []gc.Token) bool {
	switch toks[0].Ch {
	case gc.CONST, gc.VAR, gc.FUNC, gc.TYPE:
		if s := toks[1].Src(); token.IsIdentifier(s) && token.IsExported(s) {
			return true
		}
	}
	return false
}

func (c *cfg) sep(s string) []byte {
	if s == "" {
		return nil
	}

	if strings.IndexByte(s, '\n') >= 0 {
		return nl
	}

	return sp
}
