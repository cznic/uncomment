// Copyright 2022 The Uncomment Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Command uncomment removes comments from Go source files.
//
// Installation
//
// To install/update:
//
//	$ go install modernc.org/uncomment@latest
//
// Verbose output
//
// Add -v to turn on progress tracing and printing out totals.
//
// Keeping godoc comments
//
// Add -keep-godoc to keep comments of exported top level declarations.
//
// Formatting output
//
// Add -gofmt to format the output files.
//
// Example
//
// To remove non-godoc comments in a set of specific files and format the result:
//
//	uncomment -keep-godoc -gofmt lib/sqlite*.go libtest/sqlite*.go
//
// Notes
//
// Input files are overwritten without asking. Be sure to have a backup of your
// files before using uncomment.
//
// Package comments are not touched.
//
// Bugs
//
// The parser used by uncomment does not yet handle some generic constructs.
package main // import "modernc.org/uncomment"

import (
	"fmt"
	"os"

	"modernc.org/uncomment/engine"
)

func main() {
	if err := engine.Run(os.Args[0], os.Args[1:], os.Stderr); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
